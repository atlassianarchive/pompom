(function(global, undefined) {

  // This assumes locale is en.

  var thousandSeparator = ',';
  var decimalSeparator = '.';
  var defaultDecimals = 2;

  var formatNumber = global.formatNumber = function(number, decimals) {
    var sign = number < 0 ? '-' : '';
    var decimals = _.isUndefined(decimals) ? defaultDecimals : decimals;
    var dollars = Math.abs(+number);
    var integral = parseInt(dollars.toFixed(decimals), 10).toString();
    var cents = Math.abs(dollars - integral).toFixed(decimals);
    var first = integral.length > 3 ? integral.length % 3 : 0;
    return sign
         + (first ? integral.substr(0, first) + thousandSeparator : '')
         + integral.substr(first).replace(/(\d{3})(?=\d)/g, '$1' +  thousandSeparator)
         + (decimals ? decimalSeparator + cents.slice(2) : '');
  };

  global.formatPercentage = function(number, decimals) {
    return formatNumber(number, decimals) + '%';
  };

  global.formatMoney = function(amount, symbol, decimals) {
    var symbol = symbol || '$';
    return symbol + formatNumber(amount, decimals);
  };

}(window, undefined));