module.exports = function(config, dependencies, job_callback) {
    var request = dependencies.request;
    var async = dependencies.async;
    var _ = dependencies.underscore;

    function pluginFeedback(pluginKey, callback) {
        var options = {
          url: config.globalAuth['mpac'].api + '/plugins/' + pluginKey + '/feedback',
          auth: {
            user: config.globalAuth['mpac'].username,
            pass: config.globalAuth['mpac'].password,
            sendImmediately: true
          }
        };

        request(options, function(error, response, body) {
            var data = JSON.parse(body);
            
            var feedback = _.chain(data.feedback)
                .map(function(f) {
                    f.message = f.reasonCode === 'upm.feedback.other' ? f.message : config.messages[f.reasonCode];
                    return f;
                })
                .sortBy(function(f) {
                    var date = new Date(f.time);
                    return date.getTime();
                })
                .value();

            return error ? callback(error) : callback(null, feedback);
        });
    }

    async.map(config.globalAuth.identity.pluginKeys, pluginFeedback, function(err, results) {
        var feedback = _(results).flatten().slice(0, config.limit);
        job_callback(null, { title: config.widgetTitle, feedback: feedback });
    });
};
