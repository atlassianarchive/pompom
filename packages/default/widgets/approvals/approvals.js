widget = {
    onData: function(el, data) {
        var approvalsTemplate = Mustache.compile($('#approvals-template').html());
        var issues = _.sortBy(data.approvals.issues, function(issue) {
            return issue.fields.status.name;
        });
        
        var summaries = _(issues).map(function(issue) {
            return {
                summary: issue.fields.summary.substring("Marketplace Approval for".length),
                status: issue.fields.status.name
            };
        });

        $('.content', el).html(approvalsTemplate({ approvals: summaries }));
    }
};