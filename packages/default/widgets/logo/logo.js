widget = {
    onData: function(el, data) {
        var logoTemplate = Mustache.compile($('#logo-template').html());

        $('h2', el).text(data.title);
        $('.content', el).html(logoTemplate(data));

        // hack to vertical align logo
        $(el).css('height', '100%');
    }
};