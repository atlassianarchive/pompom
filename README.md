# Atlassian Marketplace Wallboard

# Current widgets

- Total sales for the current week
- Total sales broken down per add-on for the past X weeks
- Last X UPM feedback events
- Last X add-on reviews
- Add-on approval queue
- Latest Atlassian Answers questions
- Vendor logo
- More! [Add your own](https://bitbucket.org/atlassian/atlasboard/wiki/Home)

# Installation

Make sure node.js is installed then:

1. Install [Atlasboard](http://atlasboard.bitbucket.org/)
2. Configure the wallboard by copying `globalAuth.json.sample` to `globalAuth.json` and editing it.
3. `npm start` to start the dashboard.